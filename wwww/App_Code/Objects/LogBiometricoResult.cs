﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LogBiometricoResult
/// </summary>
public class LogBiometricoResult
{
    public LogBiometricoResult()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int ID { get; set; }
    public string Names { get; set; }
    public string Identification { get; set; }
    public string Message { get; set; }
    public DateTime Entry { get; set; }
    public string linkfoto { get; set; }
    public string sucursal { get; set; }

}