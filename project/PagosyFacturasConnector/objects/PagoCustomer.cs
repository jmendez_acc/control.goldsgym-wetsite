﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector.objects
{
    public class PagoCustomer
    {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public string NroComprobante { get; set; }
        public string AuthCode { get; set; }
        public string Tarjeta { get; set; }
        public string TipoCredito { get; set; }
        public string Total { get; set; }
        public string ClaveAcceso { get; set; }
    }
}
