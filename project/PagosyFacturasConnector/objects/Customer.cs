﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector.objects
{
    public class Customer
    {
        public int ID { get; set; }
        public string Nombres { get; set; }
        public string TipoIdentificacionID { get; set; }
        public string Identificacion { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Clave { get; set; }
        public string ruc_empresa { get; set; }
    }
}
