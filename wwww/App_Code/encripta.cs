﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;


public static class encripta
{
   
    public static string encrypt(string text)
    {
      
        MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
       
        Byte[] hashedBytes;
        
        UTF8Encoding encoder = new UTF8Encoding();

        StringBuilder sb = new StringBuilder();
        hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(text));

        for (int i = 0; i < hashedBytes.Length; i++)

            {

              //to make hex string use lower case instead of uppercase add parameter “X2″

                sb.Append(hashedBytes[i].ToString("X2"));

            }

        return sb.ToString();

    }
		 
}