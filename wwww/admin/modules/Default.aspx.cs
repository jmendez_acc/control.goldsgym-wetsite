﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;


public partial class ADMIN_administrador_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        showAdmin();
   
        if (!IsPostBack)
        {
          
            //this.literalUserName.Text = HttpContext.Current.User.Identity.Name;
        }
    }
    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
      
        if (e.Argument == "InitialPageLoad")
        {
            //simulate longer page load
            System.Threading.Thread.Sleep(2000);
            RadSplitter1.Visible = true;
        }
    }


    protected void showAdmin()
    {

            adminMenu.Visible = true;


    }


}