﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ADMIN_administrador_Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<html xmlns='https://www.w3.org/1999/xhtml'>
    <meta http-equiv="X-UA-Compatible" content="IE=100" /> <!-- IE8 mode -->
    <meta name="viewport" content="width=device-width, initial-scale=1" /><meta charset="utf-8" />
<%--    <meta http-equiv="Refresh" content="325;URL=../index.aspx">--%>
<head>
   <title>ΔCCROΔCHCODE | Content management system</title>
     <link rel="Shortcut Icon" type="//www.accroachcode.com/images/accroachcode.ico" href="//www.accroachcode.com/images/accroachcode.ico" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <style type="text/css">
        html, body, form
        {
            height:100%;
            margin:0px;
            padding:0px;
        }
        
        #<%= Panel1.ClientID %>,
        #<%= Panel1.ClientID %>Panel
        {
            height:100%;
        }
    </style>
        <STYLE TYPE="text/css">

TD{font-size: 16px;
    font-family: "Segoe UI",Arial,Helvetica,sans-serif;}

</STYLE>
    </telerik:RadCodeBlock>
     <script type="text/javascript">
         function OnClick(sender, args) {
             // call a method in master page

             ShowLoadingPanel();
         }
</script>

</head>
<body>
    <form id="form1" runat="server">
     
       
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">

            <script type="text/javascript">
                function pageLoad(sender, eventArgs) {
                    if (!eventArgs.get_isPartialLoad()) {
                        $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("InitialPageLoad");
               }
           }

           function ShowLoadingPanel() {
               var iframe = $find("<%= Radpane2.ClientID %>").getExtContentElement();
                $find("<%= AjaxLoadingPanel1.ClientID %>").show(iframe.id);
            }

            function HideLoadingPanel() {
                var iframe = $find("<%= Radpane2.ClientID %>").getExtContentElement();
                $find("<%= AjaxLoadingPanel1.ClientID %>").hide(iframe.id);
               
            }
            </script>
             
        </telerik:RadCodeBlock>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>

        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="AjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadAjaxLoadingPanel ID="AjaxLoadingPanel1" runat="server" Height="75px"
            Width="75px" Skin="BlackMetroTouch">
        </telerik:RadAjaxLoadingPanel>
        <asp:Panel ID="Panel1" runat="server">

            <telerik:RadSplitter ID="RadSplitter1" runat="server" Height="100%" Width="100%" Skin="BlackMetroTouch" VisibleDuringInit="false">

                <telerik:RadPane runat="server" ID="RadPane1" Width="19%" ShowContentDuringLoad="false">
                    <table style="color:#FFFFFF;">
                        <tr>
                            <td>
                                <img src="Img/User.png" /></td>
                            <td>&nbsp;&nbsp;PANEL DE CONTROL</td>
                        </tr>
                    </table>
                    <telerik:RadPanelBar ID="panelWebsite" runat="server" Width="100%" Skin="BlackMetroTouch" OnClientItemClicking="OnClick" Visible="True">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="True" Text="www.goldsgym.com.ec">
                                <Items>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/agenda.png" NavigateUrl="~/ADMIN/administrador/registrosGrid.aspx" Text="Oportunidad Laboral" Target="Radpane2">
                                    </telerik:RadPanelItem>

                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/news.png" NavigateUrl="~/ADMIN/administrador/suscripcionesGrid.aspx" Text="Suscriptores a Noticias" Target="Radpane2">
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/patrocinadores.png" NavigateUrl="~/ADMIN/administrador/Banner.aspx" Text="Banner en Home" Target="Radpane2">
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/patrocinadores.png" NavigateUrl="~/ADMIN/administrador/Noticias.aspx" Text="Noticias en Home" Target="Radpane2">
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/analitics5.png" Text="&nbsp;&nbsp;Google Analytics" Target="Radpane2" NavigateUrl="https://box.accroachcode.com/analytics/goldsgym">
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                    <telerik:RadPanelBar ID="adminMenu" runat="server" Width="100%" Skin="BlackMetroTouch" OnClientItemClicking="OnClick" Visible="False">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="True" Text="Clientes">
                                <Items>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/report.png" NavigateUrl="~/ADMIN/administrador/inscripcionesGrid2.aspx" Text="Registro de clientes" Target="Radpane2">
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                    <telerik:RadPanelBar ID="panelBiometrico" runat="server" Width="100%" Skin="BlackMetroTouch" OnClientItemClicking="OnClick" Visible="True">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="True" Text="Biometrico">
                                <Items>
                                    <telerik:RadPanelItem runat="server" ImageUrl="~/admin/administrador/img/agenda.png" NavigateUrl="~/ADMIN/administrador/logBiometrico.aspx" Text="Log Biometrico" Target="Radpane2">
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                    <!------------------system------------------------->
                    <telerik:RadPanelBar ID="RadPanelBar2" runat="server" Width="100%" Skin="BlackMetroTouch" OnClientItemClicking="OnClick">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="True" Text="Sistema">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Text="Cambiar contraseña" ImageUrl="~/ADMIN/administrador/img/password4.png" Target="Radpane2" NavigateUrl="~/ADMIN/administrador/changePassword.aspx">
                                    </telerik:RadPanelItem>
                                    <telerik:RadPanelItem runat="server" Text="Salir de Sesión" NavigateUrl="~/ADMIN/administrador/salir.aspx">
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                    <!------------------end------------------------->
                </telerik:RadPane>
                <telerik:RadSplitBar runat="server" ID="RadSplitbar1" CollapseMode="Forward" Skin="BlackMetroTouch" />
                <telerik:RadPane runat="server" ID="Radpane2" Width="70%" ContentUrl="welcome.html">
                </telerik:RadPane>
            </telerik:RadSplitter>
        </asp:Panel>
    </form>
    <style>
        #RAD_SPLITTER_PANE_CONTENT_RadPane1{
            background:#000000;
        }
        .RadPanelBar_BlackMetroTouch .rpRootLink {
            color: #000000;
            background-color:#fbe605;
        }
    </style>
</body>
</html>