﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /*
            Authentication control
            */

            //DEVELOPMENT
           /*
                 Session["admin_role_id"] = "1";
                 Session["admin_user_id"] = "1";
                 Session["admin_user_name"] = "juan méndez";
                 loadNavigation();
                 loadUserInfo();
           
        */
           //PRODUCTION
           
           if (Session["admin_user_id"] != null)
           {
               loadNavigation();
               loadUserInfo();
           }
           else
           {
               Response.Redirect("~/admin/login");
           }
           
        }
    }

    public void loadUserInfo()
    {
        this.literalProfilePicture.Text = @"<img src='assets/images/images.png' class='demo-avatar'> ";
        this.literalProfileName.Text = Session["admin_user_name"].ToString().Trim();
    }
    public void loadNavigation()
    {


        try
        {
            if (Session["IsAdmin"].ToString().CompareTo("1") == 0)  //admin
            {
                literalNav.Text = @"
                <a class='mdl-navigation__link active' href='#' rel='modules/welcome.html'><i class='fa fa-dashboard'></i><span class='menu-label'>INICIO</span></a>
                
                <a class='mdl-navigation__link parent-menu' href='#' rel='submenu-biometrico'><div class='arrow-list-bullet'><span class='glyphicon glyphicon-triangle-right' aria-hidden='true'></span></div><i class='fa fa-smile-o'></i><span class='menu-label'>Biométrico</span></a>
                    <div class='submenu-navigation' id='submenu-biometrico'>
                        <a id='report1' class='mdl-navigation__link' href='#' rel='modules/liveBiometrico.aspx?v=1'><span class='menu-label'>Live Biométrico</span></a>
                   </div>
               ";
            }
            else //cajero
            {
                literalNav.Text = @"
                <a class='mdl-navigation__link active' href='#' rel='modules/welcome.html'><i class='fa fa-dashboard'></i><span class='menu-label'>INICIO</span></a>
                <a class='mdl-navigation__link parent-menu' href='#' rel='submenu-biometrico'><div class='arrow-list-bullet'><span class='glyphicon glyphicon-triangle-right' aria-hidden='true'></span></div><i class='fa fa-smile-o'></i><span class='menu-label'>Biométrico</span></a>
                    <div class='submenu-navigation' id='submenu-biometrico'>
                        <a id='report1' class='mdl-navigation__link' href='#' rel='modules/liveBiometrico.aspx?v=1'><span class='menu-label'>Live Biométrico</span></a>
                   </div>
               ";
            }
            

        }
        catch (Exception ex)
        {
            //in case role id is not clear we hide the grid

        }



    }
}