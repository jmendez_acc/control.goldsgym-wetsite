﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ADMIN_administrador_changePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["admin_user_id"] == null) { Utils.cierraSesion(Response); }
        }
    }
    protected void RadButton1_Click(object sender, EventArgs e)
    {
        if (txt_clave.Text.CompareTo(txt_re_clave.Text) == 0)
           
             
        {
            goldsgymEntities db = new goldsgymEntities();
            string currentUser = HttpContext.Current.User.Identity.Name;
            string currentPassMd5 = encripta.encrypt(txt_clave_actual.Text);

            user_admin user = db.user_admin.SingleOrDefault(p=>p.usuario==currentUser && p.clave== currentPassMd5);
            /*var currentUserDb = (from u in db.user_admin
                                where u.usuario == currentUser
                                where u.clave == currentPassMd5
                                select u).FirstOrDefault();
                                */
            if (user != null)
            {
              
                    user.clave = encripta.encrypt(txt_clave.Text);
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "alert('Clave cambianda con Exito!')", true);
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "alert('Clave actual no esta correcta')", true);
            }
            
        }
        else {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "alert('La clave debe coincidir.')", true);
        }
        clear();

    }



    private void clear()
    {
        txt_clave.Text = string.Empty;
        txt_clave_actual.Text = string.Empty;
        txt_re_clave.Text = string.Empty;
    }
}