﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="liveBiometrico.aspx.cs" Inherits="ADMIN_livebiometrico" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../assets/js/jquery-1.11.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <link href="../assets/css/style.css?v=6" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">



          <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.module-content-grid').css('visibility', 'visible');
                    parent.hideLoader();

                    setInterval(function () {                       
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        ajaxManager.ajaxRequestWithTarget('<%= btnRefresh.UniqueID %>', '');                      
                    }, 4000);
                });

                var estadoPuerta = "Off";
                function abrirPuerta() {

                    if (estadoPuerta == "Off") {
                        //alert("abrir puerta!");
                        estadoPuerta = "On";
                            $('#unlocked').hide();
                            $('#locked').show();
                            $find("<%= btnAbrirpuerta.ClientID %>").set_text("Cerrar Puerta en " + $find('<%=cmbSucursal.ClientID %>').get_selectedItem().get_text());
                    
                    }
                    else {
                        //alert("cerrar puerta!");
                        estadoPuerta = "Off";
                            $('#locked').hide();
                            $('#unlocked').show();
                            $find("<%= btnAbrirpuerta.ClientID %>").set_text("Abrir Puerta en " + $find('<%=cmbSucursal.ClientID %>').get_selectedItem().get_text());
                       
                    }
                    var data = {};
		            $.post('http://'+document.getElementById("<%= hiddenIpPuerta.ClientID %>").value+'/cm?cmnd=Power%20'+estadoPuerta+'&user=admin&password=hcaorccacode', data)
	                .done(function(submitResponse) {
	           
	                }, 'json')
	                .fail( function(xhr, textStatus, errorThrown) {      
                        });
                    
                }


                function togglePuerta() {
                    $('#<%=btnAbrirpuerta.ClientID%>')[0].control.set_enabled(false);
                    abrirPuerta();
                    setTimeout(function () {
                        abrirPuerta();
                        $('#<%=btnAbrirpuerta.ClientID%>')[0].control.set_enabled(true);
                    }, 4000);
                }


                function showIconPuerta() {
                    if (estadoPuerta == "Off") {
                        $('#locked').hide();
                        $('#unlocked').show();
                    }
                    else {
                        $('#unlocked').hide();
                        $('#locked').show();
                    }
                }


                function hideAllIconPuerta() {
                    $('#unlocked').hide();
                    $('#locked').hide();
                }
            </script>

            

        </telerik:RadCodeBlock>



        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" Runat="server" Skin="MetroTouch" DecoratedControls="All" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnRefresh">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" UpdatePanelCssClass="" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="cmbSucursal">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="btnAbrirPuerta" UpdatePanelCssClass="" />
                        <telerik:AjaxUpdatedControl ControlID="hiddenIpPuerta" UpdatePanelCssClass="" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="BlackMetroTouch">
        </telerik:RadAjaxLoadingPanel>















        <div class="row module-header">
            <div class="col-xs-8 text-left">
                <h3>Pantalla Biométrico
                </h3>
            </div>
            <div class="col-xs-4 text-right" runat="server" id="createProyectActionDiv" style="margin-top: -11px">
            </div>
        </div>

        <div class="module-content-grid" style="visibility: visible">

            

            <div style="display:none">
<telerik:RadButton ID="btnRefresh" runat="server" OnClick="btnRefresh_Click" Text="refrescar" Skin="Material" RenderMode="Lightweight"></telerik:RadButton>
                <asp:HiddenField ID="hiddenIpPuerta" runat="server" />
                    </div>



            <div class="container module" style="padding:0px">
                <div class="toolbar">
                    
                    <div class="row">
                        <div class="col-xs-6">
                            <telerik:RadComboBox ID="cmbSucursal" runat="server" Skin="Material" RenderMode="Lightweight" AutoPostBack="true" Width="400px" OnSelectedIndexChanged="cmbSucursal_SelectedIndexChanged">
                    </telerik:RadComboBox>
                        </div>
                        <div class="col-xs-6 text-right">
                            
                            <table style="float:right">
                                <tr>
                                    <td style="padding-right:15px;">
                                            <img id="locked" src="../assets/images/padunlock.png" style="width:35px;display:none"/>
                                            <img id="unlocked" src="../assets/images/padlock.png" style="width:35px;display:none"/>
                                        
                                    </td>
                                    <td>
                                        <telerik:RadButton ID="btnAbrirpuerta" runat="server" Skin="Material" Visible="false" RenderMode="Lightweight" Text="Abrir Puerta" AutoPostBack="false" OnClientClicked="togglePuerta"></telerik:RadButton>
                                    </td>
                                </tr>
                            </table>
                         </div>
                    </div>
                </div>
                <telerik:RadGrid ID="RadGrid1"
                                runat="server"
                                Width="100%"
                                CellSpacing="-1"
                                Skin="Material"
                                RenderMode="Lightweight"
                                OnNeedDataSource="RadGrid1_NeedDataSource"
                                AllowPaging="True"
                                AllowCustomPaging="true"
                                AllowSorting="True"
                                PageSize="15">
                                <PagerStyle Mode="NextPrevandNumeric" Position="Bottom"></PagerStyle>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="False" UseStaticHeaders="False" EnableVirtualScrollPaging="False" />
                                </ClientSettings>
                                <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" >
                                  
                                    <Columns>


                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>

                                                <div class="row" style="margin-bottom:15px;margin-top:15px">
                                                    <div class="col-xs-4 text-right">
                                                        <a onclick="MyWindow=window.open('<%# Eval("linkfoto") %>','MyWindow','width=650,height=490'); return false;" href="#" target=""><img src="<%# Eval("linkfoto") %>" style="height: auto; width: 200px; border-radius: 0px" /></a>
                                                    </div>
                                                    <div class="col-xs-8" style="font-size: 1.3em;padding-top: 5px;">
                                                        <div class="row">
                                                            <div class="col-xs-3 text-right">
                                                                <strong>SUCURSAL:</strong>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <%# Eval("sucursal") %>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 text-right">
                                                                <strong>IDENTIFIC:</strong>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <%# Eval("Identification") %>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 text-right">
                                                                <strong>NOMBRES:</strong>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <%# Eval("Names") %>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-xs-3 text-right">
                                                                <strong>FECHA Y HORA:</strong>
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <%# ((DateTime)Eval("Entry")).ToString("dd/MM/yyyy h:mm:ss tt") %>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 text-right">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <%# Eval("Message") %>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>





                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>



                
            </div>
        </div>
    </form>
</body>
</html>
