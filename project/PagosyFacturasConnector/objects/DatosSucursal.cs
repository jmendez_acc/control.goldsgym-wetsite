﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector.objects
{
    public class DatosSucursal
    {
        public string codigoSucursal { get; set; }
        public string codigoPtoEmision { get; set; }
        public string direccionMatriz{get;set; }
        public string direccionSucursal { get; set; }
    }
}
