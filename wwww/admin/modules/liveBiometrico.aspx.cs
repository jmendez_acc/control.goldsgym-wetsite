﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class ADMIN_livebiometrico : System.Web.UI.Page
{



    goldsgymEntities db = new goldsgymEntities();
    IQueryable<LogBiometrico> logQuery = null;
    List<LogBiometricoResult> listaLog = new List<LogBiometricoResult>();
    PagosyFacturasConnector.DatamodelDataContext dbFacturacion = new PagosyFacturasConnector.DatamodelDataContext();
    int _sucursalID = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["admin_user_id"] == null) { Utils.cierraSesion(Response); }
                loadSucursales();
            }
            catch (Exception) {
                Utils.cierraSesion(Response);
            }            
        }
    }



    public void loadSucursales()
    {
        if (Session["IsAdmin"].ToString().CompareTo("1") == 0) {
            var sucursales = from p in db.Sucursales where p.Active == true select p;
            cmbSucursal.Items.Add(new RadComboBoxItem("[TODAS LAS SUCURSALES]", "0"));
            foreach (Sucursale suc in sucursales)
            {
                cmbSucursal.Items.Add(new RadComboBoxItem(suc.Nombre, suc.ID.ToString()));
            }

        }
        else
        {
            var sucursalesPermitidas = dbFacturacion.getEstablecimientosUsuario(int.Parse(Session["admin_user_id"].ToString()));
            int i = 0;
            foreach (PagosyFacturasConnector.getEstablecimientosUsuarioResult sucursalPermitida in sucursalesPermitidas)
            {
                
                var sucursales = from p in db.Sucursales where p.PYF_EstablecimientoID==sucursalPermitida.ID && p.Active == true select p;                
                foreach (Sucursale suc in sucursales)
                {
                    cmbSucursal.Items.Add(new RadComboBoxItem(suc.Nombre, suc.ID.ToString()));
                    if (i == 0)
                    {
                        setButtonText(suc.ID,suc.Nombre);
                        setIpPuerta(suc.ID);
                        
                    }
                }
                i++;

            }
        }
        
    }

   
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        int pageIndex = RadGrid1.CurrentPageIndex;

        /*if (filterNumeroComprobante.CompareTo(this.RadGrid1.MasterTableView.GetColumn("numeroComprobante").CurrentFilterValue) != 0 ||
            filterRucReceptor.CompareTo(this.RadGrid1.MasterTableView.GetColumn("rucReceptor").CurrentFilterValue) != 0 ||
            filterClienteNombre.CompareTo(this.RadGrid1.MasterTableView.GetColumn("razonSocialReceptor").CurrentFilterValue) != 0)
        {
            pageIndex = 0;
        }

        filterNumeroComprobante = this.RadGrid1.MasterTableView.GetColumn("numeroComprobante").CurrentFilterValue;
        filterRucReceptor = this.RadGrid1.MasterTableView.GetColumn("rucReceptor").CurrentFilterValue;
        filterClienteNombre = this.RadGrid1.MasterTableView.GetColumn("razonSocialReceptor").CurrentFilterValue;


        GridColumn column = RadGrid1.MasterTableView.GetColumnSafe("codDoc");
        GridColumn columnEstado = RadGrid1.MasterTableView.GetColumnSafe("Estado");
        */
        


        if (cmbSucursal.SelectedValue == "0")
        {            
            logQuery = (from p in db.LogBiometricoes orderby p.Entry descending select p).Take(5);            
        }
        else
        {
            _sucursalID = int.Parse(cmbSucursal.SelectedValue);
            logQuery = (from p in db.LogBiometricoes where p.SucursalID== _sucursalID orderby p.Entry descending select p).Take(5);
        }

        string mensaje = "";
        //RadGrid1.DataSource = log;
        foreach (LogBiometrico log in logQuery)
        {
            LogBiometricoResult newLog = new LogBiometricoResult();
            newLog.ID = log.ID;
            newLog.Names = log.Names;
            newLog.sucursal = log.Sucursale.Nombre;
            newLog.Identification = log.Identification;

            mensaje = getMensaje((bool)log.OpenDoor, log.Message);

            newLog.Message = mensaje;
            newLog.Entry = (DateTime)log.Entry;
            newLog.linkfoto = getLinkfoto((int)log.UserID, log.Identification);
            listaLog.Add(newLog);
        }
        RadGrid1.DataSource = listaLog;

        //cargaGridComprobantes(false, pageIndex, RadGrid1.PageSize);
    }





    public string getMensaje(bool opendoor, string mensaje)
    {

        string msg = mensaje.Replace("\n", "||");

        string res = "";

        if (msg.IndexOf("||") > 0)
        {
            string line1 = msg.Substring(0, msg.IndexOf("||"));
            string line2 = msg.Substring(msg.IndexOf("||")+2);
            if (opendoor)
            {
                res = "<strong style='color:#0aaa0a'>" + line1 + "</strong><br/>" + line2;
            }
            else
            {
                res = "<strong style='color:#ff0000'>" + line1 + "</strong><br/>" + line2;
            }
        }
        else
        {
            if (opendoor)
            {
                res = "<strong style='color:#0aaa0a'>" + mensaje + "</strong>";
            }
            else
            {
                res = "<strong style='color:#ff0000'>" + mensaje + "</strong>";
            }
        }
        

        return res;

    }


    public string getLinkfoto(int userID, string identifiction)
    {
        string res = "";
        if (identifiction != null)
        {
            if (identifiction != "" && userID != 0)
            {
                res = "https://www.goldsgym.com.ec/Content/biometrics/users/" + userID.ToString() + "/" + identifiction + "_nogesture.jpg";
            }
            else
            {
                res = "https://www.goldsgym.com.ec/Content/biometrics/avatar_default.jpg";
            }
        }
        return res;
    }



    public void setButtonText(int sucursalID,string nombreSucursal)
    {
        if (sucursalID == 0)
        {
            btnAbrirpuerta.Visible = false;
            btnAbrirpuerta.Text = "Abrir puerta en " + nombreSucursal;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "textx", "hideAllIconPuerta();", true);
        }
        else
        {
            btnAbrirpuerta.Visible = true;
            btnAbrirpuerta.Text = "Abrir puerta en " + nombreSucursal;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "showIconPuerta();", true);
        }
        
    }

    public void setIpPuerta(int sucursalID) {
        Sucursale sucursal = db.Sucursales.FirstOrDefault(p=>p.ID==sucursalID);
        if (sucursal != null)
        {
            hiddenIpPuerta.Value = sucursal.IpPuerta1;
        }
    }


    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        RadGrid1.Rebind();
    }



    protected void cmbSucursal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        setButtonText(int.Parse(cmbSucursal.SelectedValue),cmbSucursal.SelectedItem.Text);
        setIpPuerta(int.Parse(cmbSucursal.SelectedValue));
        btnAbrirpuerta.Enabled = true;
    }


}