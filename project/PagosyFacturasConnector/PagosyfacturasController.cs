﻿using PagosyFacturasConnector.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector
{
    public class PagosyfacturasController
    {
        int user_id = 21;  //ID DE USUARIO DE GOLDSGYM 
        string ruc_empresa = "0992222980001"; //RUC DE GOLDSGYM
        string razonSocial = "WUTH & ASOCIADOS WUTHSA S.A.";
        string tokenPassword = "^(zPzV@j~4kJ,9";
        public List<objects.Sucursal> getSucursales() {
            List<objects.Sucursal> res=new List<objects.Sucursal>();
            DatamodelDataContext db = new DatamodelDataContext();
            var sucursales = from p in db.establecimientos where p.RUC_EMPRESA == ruc_empresa select p;
            foreach(establecimientos e in sucursales)
            {
                objects.Sucursal sucursal = new objects.Sucursal();
                sucursal.ID = e.ID;
                sucursal.Nombre = e.NOMBRE;
                sucursal.Direccion = e.DIRECCION;
                sucursal.Codigo = e.COD_ESTABLECIMIENTO;
                res.Add(sucursal);
            }
            return res;
        }


        public void fixClientesBinding()
        {
           

            
        }
        
        public List<objects.Producto> getProductosBySucursal(int establecimientoID) {

            List<objects.Producto> res = new List<objects.Producto>();
            DatamodelDataContext db = new DatamodelDataContext();

            var productos_web = from p in db.productos_webs where p.ID_ESTABLECIMIENTO == establecimientoID select p;
            foreach (productos_web pw in productos_web) {
                objects.Producto sucursal = new objects.Producto();
                sucursal.ID = (int)pw.ID_PRODUCTO;
                sucursal.Nombre = pw.ACC_PRODUCTOS.DESC_PRODUCTO;
                sucursal.Codigo = pw.ACC_PRODUCTOS.COD_PRODUCTO;
                if (pw.PRECIO != null)
                {
                    sucursal.Precio = Math.Round((decimal)pw.PRECIO, 2);
                }
                else
                {

                    sucursal.Precio = Math.Round((decimal)pw.ACC_PRODUCTOS.PRECIO, 2);
                }
                res.Add(sucursal);
            }


            /*
            List < int > listaIDSpermitidos = new List<int>();
            listaIDSpermitidos.Add(735);  //DIARIO
            listaIDSpermitidos.Add(736);  //SEMANA
            listaIDSpermitidos.Add(737);  //MEMBRESIA MENSUAL 11 A 16
            listaIDSpermitidos.Add(738);  //MENSUAL
            listaIDSpermitidos.Add(739);  //MENSUAL 2 MESES
            listaIDSpermitidos.Add(740);  //TRIMESTRAL
            listaIDSpermitidos.Add(741);  //TETRAMENSUAL 4 MESES
            listaIDSpermitidos.Add(742);  //SEMESTRAL
            listaIDSpermitidos.Add(743);  //ANUAL
            listaIDSpermitidos.Add(745);  //TRIMESTRAL ESTUDIANTES (3 GYM)
            listaIDSpermitidos.Add(746);  //MENSUAL FF.AA.
            listaIDSpermitidos.Add(747);  //TRIMESTRAL FF.AA.
            listaIDSpermitidos.Add(748);  //MENSUAL ESTUDIANTES
            listaIDSpermitidos.Add(758);  //MENSUAL ESTUDIANTES (3 GYM)


            

            var productos = from p in db.ACC_PRODUCTOS where listaIDSpermitidos.ToArray().Contains(p.ID_PRODUCTO) select p;
            foreach (ACC_PRODUCTOS e in productos)
            {

                var restricciones = from s in db.productos_establecimientos where s.ID_PRODUCTO == e.ID_PRODUCTO select s;
                if (restricciones.Count() > 0)
                {
                    bool addit = false;
                    foreach(productos_establecimientos restriccion in restricciones)
                    {
                        if (restriccion.ID_ESTABLECIMIENTO == establecimientoID)
                        {
                            addit = true;
                            break;
                        }
                    }
                    if (addit)
                    {
                        objects.Producto sucursal = new objects.Producto();
                        sucursal.ID = e.ID_PRODUCTO;
                        sucursal.Nombre = e.DESC_PRODUCTO;
                        sucursal.Codigo = e.COD_PRODUCTO;
                        sucursal.Precio = Math.Round(e.PRECIO,2);
                        res.Add(sucursal);
                    }
                }
                else
                {
                    objects.Producto sucursal = new objects.Producto();
                    sucursal.ID = e.ID_PRODUCTO;
                    sucursal.Nombre = e.DESC_PRODUCTO;
                    sucursal.Codigo = e.COD_PRODUCTO;
                    sucursal.Precio = Math.Round(e.PRECIO,2);
                    res.Add(sucursal);
                }
                
            }
            */
            
            return res;
        }



        public objects.Producto getProductoByID(int productID) {
            objects.Producto producto = null;
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_PRODUCTOS prod = db.ACC_PRODUCTOS.SingleOrDefault(p=>p.ID_PRODUCTO==productID);
            if (prod != null)
            {
                producto = new Producto();
                producto.Nombre=prod.DESC_PRODUCTO;
                producto.Precio = prod.PRECIO;
                producto.Codigo = prod.COD_PRODUCTO;
            }
            return producto;
        }

        public objects.DatosSucursal getCodigosPuntoEmision(int establecimientoID)
        {
            objects.DatosSucursal res = null;
            DatamodelDataContext db = new DatamodelDataContext();
            establecimientos est = db.establecimientos.SingleOrDefault(p=>p.ID == establecimientoID && p.RUC_EMPRESA == ruc_empresa);
            if (est != null)
            {
                res = new objects.DatosSucursal();
                res.codigoSucursal = est.COD_ESTABLECIMIENTO;
                res.codigoPtoEmision = "003";
                res.direccionSucursal = est.DIRECCION;
                res.direccionMatriz = est.compania.DIRECCION;                        
            }
            return res;
        }

        public string isUserRegisteredInPagosyFacturas(string RUC)
        {
            //bool res = false;
            string res = "0|NOTREGISTERED";
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_CLIENTE cliente = db.ACC_CLIENTEs.FirstOrDefault(p=>p.ruc_empresa== ruc_empresa && p.IDENTIFICACION==RUC && p.ESTADO=="A");
            if (cliente != null)
            {
                res = "1|"+cliente.CORREO;
            }
            return res;
        }


        public Customer getCustomerFromPagosyFacturas(string email,string ci)
        {
            Customer res = null;
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_CLIENTE cliente = db.ACC_CLIENTEs.FirstOrDefault(o=>o.ruc_empresa==ruc_empresa && o.CORREO.Contains(email) && o.IDENTIFICACION.CompareTo(ci)==0 && o.ESTADO=="A");
            if (cliente != null)
            {
                res = new Customer();
                res.Correo = email;
                res.ID = cliente.ID_CLIENTE;
                res.Nombres = cliente.NOMBRES;
                res.Identificacion = cliente.IDENTIFICACION;
                res.ruc_empresa = cliente.ruc_empresa;
                res.Direccion = cliente.DIRECCION;
                res.Telefono = cliente.TELEFONO;
                res.TipoIdentificacionID = cliente.TIPO_IDENTIFICACION;
            }
            return res;
        }

        public Customer getCustomerFromPagosyFacturasByOnlyMail(string email)
        {
            Customer res = null;
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_CLIENTE cliente = db.ACC_CLIENTEs.SingleOrDefault(o => o.ruc_empresa == ruc_empresa && o.CORREO.Contains(email) && o.ESTADO=="A");
            if (cliente != null)
            {
                res = new Customer();
                res.Correo = email;
                res.ID = cliente.ID_CLIENTE;
                res.Nombres = cliente.NOMBRES;
                res.Identificacion = cliente.IDENTIFICACION;
                res.ruc_empresa = cliente.ruc_empresa;
                res.Direccion = cliente.DIRECCION;
                res.Telefono = cliente.TELEFONO;
                res.Clave = cliente.IDENTIFICACION;
                res.TipoIdentificacionID = cliente.TIPO_IDENTIFICACION;
            }
            return res;
        }

        public int createPaymentOrder(string subOrderID,string clienteNombre,string clienteTipoIdentificacion,string clienteRuc,string clienteEmail,string clienteTelefono,string clienteDireccion,decimal subtotal,decimal iva, decimal total,string details,
            string facturaCompradorNombre,
            string facSucursal,
            string facPtoEmi,
            string facDirMatriz,
            string facDirSucursal, 
            DateTime membresiaFechaInicio, int membresiaEstablecimientoID, string membresiaEstablecimientoNombre
            )
        {
            int res = 0;
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            PaymentOrder paymentOrder = new PaymentOrder();

            paymentOrder.SubOrderID = subOrderID;
            paymentOrder.TipoComprobante = "FAC";
            //paymentOrder.UsuarioLogo = idEmpresa.logo;
            paymentOrder.UsuarioNombre = razonSocial;
            paymentOrder.UsuarioRazonSocial = razonSocial;
            paymentOrder.CustomerNombre = clienteNombre;
            //paymentOrder.NroComprobante = nroComprobanteForLink;
            paymentOrder.Emails = clienteEmail;

            paymentOrder.Phone = clienteTelefono;
            paymentOrder.Address = clienteDireccion;

                       

            paymentOrder.Hash = getHash(user_id.ToString()); //id de usuario de golds
            paymentOrder.UsuarioID = user_id;
            paymentOrder.UsuarioRUC = ruc_empresa;
            paymentOrder.Subtotal = subtotal;
            paymentOrder.Iva = iva;
            paymentOrder.Total = total;
            paymentOrder.Ice = 0;
            paymentOrder.Tax1 = 0;
            paymentOrder.Tax2 = 0;
            paymentOrder.Interes = 0;
            paymentOrder.TotalConInteres = 0;
            paymentOrder.Details = details;
            paymentOrder.FechaEmision = DateTime.Now;
            paymentOrder.Status = "PE";
            paymentOrder.Counter = 1;
            paymentOrder.SendComprobante = true;
            paymentOrder.Active = true;
            paymentOrder.Entry = DateTime.Now;

            paymentOrder.ComisionPfPrct = getPorcentajePagosyFacturas(ruc_empresa);

            paymentOrder.UsuarioLogo = "--";  //ESTO HAY QUE SACAR Y UNIFICAR CON LA TABLA COMPANIES

            paymentOrder.FacAmbiente = "2";  //1 DESARROLLO, 2 PRODUCCION

            paymentOrder.FacCompradorNombre = facturaCompradorNombre;


            paymentOrder.FacCompradorTipoIde = clienteTipoIdentificacion; //repeated field asignation
            paymentOrder.FacCompradorRuc = clienteRuc;
            paymentOrder.CustomerTipoIdent = clienteTipoIdentificacion; //repeated field asignation
            paymentOrder.CustomerIdentification = clienteRuc;


            paymentOrder.FacSucursal = facSucursal;
            paymentOrder.FacPtoEmision = facPtoEmi;
            paymentOrder.FacDirMatriz = facDirMatriz;
            paymentOrder.FacDirSucursal = facDirSucursal;

            if (membresiaFechaInicio != null)
            {
                paymentOrder.MembresiaDateStart = membresiaFechaInicio;
            }
            if (membresiaEstablecimientoNombre != null)
            {
                paymentOrder.MembresiaEstablecNom = membresiaEstablecimientoNombre;
            }
            if (membresiaEstablecimientoID != 0)
            {
                paymentOrder.MembresiaEstablecID = membresiaEstablecimientoID;
            }


            db.PaymentOrders.InsertOnSubmit(paymentOrder);
            db.SubmitChanges();
            res = paymentOrder.ID;
            return res;
        }

        public void updatePaymentOrder(int paymentOrderID,string details,decimal subtotal,decimal iva, decimal total,DateTime membresiaFechaInicio,int membresiaEstablecimientoID,string membresiaEstablecimientoNombre)
        {
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            PaymentOrder orden = db.PaymentOrders.SingleOrDefault(p=>p.ID==paymentOrderID);
            if (orden != null)
            {
                orden.Details = details;
                orden.Iva = iva;
                orden.Subtotal = subtotal;
                orden.Total = total;
                if (membresiaFechaInicio != null) {
                    orden.MembresiaDateStart = membresiaFechaInicio;
                }
                if(membresiaEstablecimientoNombre != null)
                {
                    orden.MembresiaEstablecNom = membresiaEstablecimientoNombre;
                }
                if (membresiaEstablecimientoID != 0)
                {
                    orden.MembresiaEstablecID = membresiaEstablecimientoID;
                }
                db.SubmitChanges();
            }
        }




        /*public void createPaymentOrder(string details,string email, decimal subtotal, decimal iva, decimal total)
        {
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();

            PaymentOrder orden = new PaymentOrder();
            orden.Details = details;
            orden.Iva = iva;
            orden.Subtotal = subtotal;
            orden.Total = total;
            orden.Status = "PE";
            orden.Emails = email;
            orden.Counter = 0;
            orden.Entry = DateTime.Now;

           


            db.PaymentOrders.InsertOnSubmit(orden);
            db.SubmitChanges();


        
        }*/

        public PaymentOrder getPaymentOrderByID(int paymentOrderID)
        {
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            PaymentOrder orden = db.PaymentOrders.SingleOrDefault(p => p.ID == paymentOrderID);
            if (orden != null)
            {
                return orden;
            }
            else
            {
                return null;
            }
        }


        public void deletePaymentOrder(int paymentOrder)
        {
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            PaymentOrder orden = db.PaymentOrders.SingleOrDefault(p => p.ID == paymentOrder);
            if (orden != null)
            {
                db.PaymentOrders.DeleteOnSubmit(orden);
                db.SubmitChanges();
            }
        }

        public void createCustomerInPagosyFacturas(string identificacion,string tipoIdentificacionID,string nombres,string email,string direccion,string celular)
        {
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_CLIENTE cliente = db.ACC_CLIENTEs.FirstOrDefault(p => p.ruc_empresa == ruc_empresa
            && p.IDENTIFICACION == identificacion
            && p.ESTADO == "A" );
            if (cliente == null)
            {
                ACC_CLIENTE clienteNuevo = new ACC_CLIENTE();
                clienteNuevo.IDENTIFICACION = identificacion;
                clienteNuevo.TIPO_IDENTIFICACION = tipoIdentificacionID;
                clienteNuevo.NOMBRES = nombres;
                clienteNuevo.CORREO = email;
                clienteNuevo.DIRECCION = direccion;
                clienteNuevo.TELEFONO = celular;
                clienteNuevo.ESTADO = "A";
                clienteNuevo.FECHA_REGISTRO = DateTime.Now;
                clienteNuevo.ruc_empresa = ruc_empresa;
                db.ACC_CLIENTEs.InsertOnSubmit(clienteNuevo);
                db.SubmitChanges();
            }
        }



        public void updateCustomerInPagosyFacturas(string identificacion, string nombres, string direccion, string celular)
        {
            DatamodelDataContext db = new DatamodelDataContext();

            ACC_CLIENTE cliente = db.ACC_CLIENTEs.FirstOrDefault(p => p.ruc_empresa == ruc_empresa
            && p.IDENTIFICACION == identificacion
            && p.ESTADO == "A");
            if (cliente != null)
            {
                //cliente.IDENTIFICACION = identificacion;
                //cliente.TIPO_IDENTIFICACION = tipoIdentificacionIDACC_CLIENTE
                cliente.NOMBRES = nombres;
                cliente.DIRECCION = direccion;
                cliente.TELEFONO = celular;
                cliente.ESTADO = "A";
                //cliente.FECHA_REGISTRO = DateTime.Now;
                db.SubmitChanges();
            }
        }



        public string getHash(string userID)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[6];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return "golds"+finalString + userID;
        }

        
        public IQueryable<PaymentOrder> getOrdenesPago(string rucCustomer,DateTime _from,DateTime _to) { 
     

            PagosyfacturasDataModelDataContext pagosDB = new PagosyfacturasDataModelDataContext();
            var paymentOrders = from p in pagosDB.PaymentOrders
                                 where p.UsuarioID == user_id
                                 && p.FacCompradorRuc == rucCustomer
                                 && p.Active == true
                                 && p.Entry >= _from
                                 && p.Entry <= _to.AddDays(1)
                                 && p.Status=="PE"
                                 orderby p.Entry descending
                                 select p;
            
            return paymentOrders;
        }


        public List<objects.ComprobanteCustomer> getComprobantesByCustomer(string rucCustomer, DateTime _from, DateTime _to) {
            List<objects.ComprobanteCustomer> res = new List<ComprobanteCustomer>();
            DatamodelDataContext db = new DatamodelDataContext();
            var comprobantes = from c in db.comprobantes where 
                               c.rucReceptor == rucCustomer 
                               && c.compania == ruc_empresa
                               && (c.estado =="N" || c.estado=="A")
                               && c.fecha >= _from
                               && c.fecha <= _to.AddDays(1)
                               select c;
            foreach (comprobante comp in comprobantes) {
                objects.ComprobanteCustomer comprobante = new objects.ComprobanteCustomer();
                comprobante.ClaveAcceso = comp.claveDeAcceso;
                comprobante.NroComprobante = comp.numeroComprobante;
                comprobante.Fecha = comp.fecha.ToString();
                comprobante.Detalle = "";
                switch (comp.codDoc)
                {
                    case "01": //facturas
                        string[] nroComp = comprobante.NroComprobante.Split('-');
                        var detalles = from v in db.vw_facturas where v.numeroruc == ruc_empresa && v.codDoc == comp.codDoc && v.estab == nroComp[0] && v.ptoEmi == nroComp[1] && v.secuencial == nroComp[2] select v;
                        int i = 0;
                        foreach (vw_factura vwf in detalles)
                        {
                            if (i != 0) {
                                comprobante.Detalle += ", ";
                            }
                            comprobante.Detalle += vwf.descripcion;
                            i++;
                        }
                        break;
                    case "04": //notas de credito
                        string[] nroComp2 = comprobante.NroComprobante.Split('-');
                        var detalles2 = from v in db.vw_ncs where v.numeroruc == ruc_empresa && v.codDoc == comp.codDoc && v.estab == nroComp2[0] && v.ptoEmi == nroComp2[1] && v.secuencial == nroComp2[2] select v;
                        int j = 0;
                        foreach (vw_nc vwf in detalles2)
                        {
                            if (j != 0)
                            {
                                comprobante.Detalle += ", ";
                            }
                            comprobante.Detalle += vwf.descripcion;
                            j++;
                        }
                        break;
                    case "07": //retencion
                        string[] nroComp3 = comprobante.NroComprobante.Split('-');
                        var detalles3 = from v in db.vw_retenciones where v.numeroruc == ruc_empresa && v.codDoc == comp.codDoc && v.estab == nroComp3[0] && v.ptoEmi == nroComp3[1] && v.secuencial == nroComp3[2] select v;
                        int k = 0;
                        foreach (vw_retencione vwf in detalles3)
                        {
                            if (k != 0)
                            {
                                comprobante.Detalle += ", ";
                            }
                            comprobante.Detalle += vwf.numDocSustento; //solamente la referencia al comprobante emitido para esa retenci'on
                            k++;
                        }
                        break;
                    case "06": //guias de remision

                        break;
                }
                comprobante.Subtotal = comp.valorTotalSinImpuesto.ToString();
                comprobante.Iva = comp.valorImpuesto.ToString();
                comprobante.Total = comp.valorTotalDocumento.ToString();
                res.Add(comprobante);
            }
            return res;
        }

        

        public List<objects.PagoCustomer> getHistorialPagosByCustomer(string rucCustomer, DateTime _from, DateTime _to)
        {
            List<objects.PagoCustomer> res = new List<PagoCustomer>();
            PagosyfacturasDataModelDataContext pagosDB = new PagosyfacturasDataModelDataContext();
            PaymentGatewayDataContext pasarelaPagosDB = new PaymentGatewayDataContext();


            var paymentOrders = from p in pagosDB.PaymentOrders
                                where p.UsuarioID == user_id
                                && p.FacCompradorRuc == rucCustomer
                                && p.Active == true
                                && p.Entry >= _from
                                && p.Entry <= _to.AddDays(1)
                                && p.Status == "CA"
                                orderby p.Entry descending
                                select p;
            foreach(PaymentOrder po in paymentOrders)
            {
                objects.PagoCustomer pago = new PagoCustomer();
                Payment payment = pasarelaPagosDB.Payments.SingleOrDefault(p=>p.OperationNumber==po.OperationNumber);
                if (payment != null)
                {
                    pago.NroComprobante = po.NroComprobante;
                    if (payment != null)
                    {
                        pago.Fecha = payment.Entry.ToString();
                        pago.AuthCode = payment.AuthCode;
                    
                    
                        if (payment.TipoCredito.CompareTo("00") == 0)
                        {
                            pago.TipoCredito = "CORRIENTE";
                        }
                        else
                        {
                            pago.TipoCredito = "DIFERIDO (" + payment.Meses.ToString() + " meses)";
                        }
                        /*switch (payment.Tarjeta)
                        {
                            case "MC":
                                pago.Tarjeta = "MasterCard";
                                break;
                            case "VI":
                                pago.Tarjeta = "Visa";
                                break;
                            case "DN":
                                pago.Tarjeta = "DinersClub";
                                break;
                            default:
                                pago.Tarjeta = "Discover";
                                break;
                        }*/
                        pago.Tarjeta = payment.Tarjeta;
                        pago.Total = payment.Total.ToString();
                    }
                }
                res.Add(pago);
            }
            
            return res;
        }


        public byte[] getRIDE(string clavedeaccesso)
        {
            //objects.Ride res = new Ride();
            WebReference.ObtieneComprobantesService cliente = new WebReference.ObtieneComprobantesService();
            //res.PDF = cliente.obtieneRide(clavedeaccesso, tokenPassword);
            return cliente.obtieneRide(clavedeaccesso, tokenPassword);
        }


        public string getXML(string clavedeaccesso)
        {
            //objects.Ride res = new Ride();
            WebReference.ObtieneComprobantesService cliente = new WebReference.ObtieneComprobantesService();
            //res.PDF = cliente.obtieneRide(clavedeaccesso, tokenPassword);
            return cliente.obtieneXML(clavedeaccesso, tokenPassword);
        }


        public System.Data.Linq.ISingleResult<getMembresiasResult> getMembresiasByClientIdentification(string clienteIdentificacion)
        {
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            //List<Membresia> res = new List<Membresia>();
            var res = db.getMembresias(ruc_empresa, clienteIdentificacion);
            //var res = from m in db.Membresias where m.RucEmpresa == ruc_empresa && m.ClientIdentification == clienteIdentificacion && m.State!="E" orderby m.Entry descending select m;  //todos menos los E de eliminados
            return res;
        }

        public List<TipoIdentificacion> getTiposIdentificacion()
        {
            DatamodelDataContext db = new DatamodelDataContext();
            List<TipoIdentificacion> res = new List<TipoIdentificacion>();

            var datos = from ti in db.ACC_TIPO_IDENTIFICACIONs where ti.TIPO_COMPROBANTE == "04" orderby ti.DESC_IDENTIFICACION select ti;
       
                foreach (ACC_TIPO_IDENTIFICACION tip in datos) {
                    TipoIdentificacion ti = new TipoIdentificacion();
                    ti.ID = tip.CODIGO_SRI;
                    ti.Nombre = tip.DESC_IDENTIFICACION;
                    res.Add(ti);
                }
            return res;

        }

        public int getCantidadCaracteresTipoDocumento(string codigo_identificacion) {
            DatamodelDataContext db = new DatamodelDataContext();
            ACC_TIPO_IDENTIFICACION tipoIdent = db.ACC_TIPO_IDENTIFICACIONs.SingleOrDefault(p => p.TIPO_COMPROBANTE == "04" && p.CODIGO_SRI == codigo_identificacion);
            if (tipoIdent != null)
            {
                return (int)tipoIdent.NUM_CARACTERES;
            }
            else
            {
                return 10;
            }

        }


        protected decimal getPorcentajePagosyFacturas(string ruc)
        {
            try
            {
                PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
                ACC_USUARIO user = db.ACC_USUARIOs.FirstOrDefault(p => p.ruc_empresa == ruc && p.id_parent == null && p.estado == "A");
                if (user != null)
                {
                    return (decimal)user.ComisionPfPrct;
                }
                else
                {
                    return 5;
                }
            }
            catch (Exception ex) {
                return 5;
            }
           
        }


        

        public string getClientsMembershipState(string clientIdentification, int sucursalID)
        {
            string result = "NO ENCONTRADA";  //por default si no encuentra ninguna membresia
            string fechaCaduca = "";
            string fechaUltimamembresia = "";
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            var resultados = db.getMembresias(ruc_empresa, clientIdentification);
            int i = 0;
            foreach (getMembresiasResult resultado in resultados)
            {
                if (resultado.ESTADO.CompareTo("ACTIVA") == 0)
                {

                    if ((bool)resultado.TodosEstablecimientos)
                    {

                        result = resultado.ESTADO;   //MEMBRESIA ACTIVA Y ACCESO PERMITIDO   
                        fechaUltimamembresia = ((DateTime)resultado.MembresiaPurchDate).ToString("dd/MM/yyyy h:mm tt");
                        fechaCaduca = ((DateTime)resultado.MembresiaEndDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        int estabID = 0;
                        //gets establecimientoID de pagosyfacturas y lo compara con el de la membresia
                        goldsgymDataContext dbGG = new goldsgymDataContext();
                        Sucursale suc = dbGG.Sucursales.FirstOrDefault(p => p.ID == sucursalID);
                        if (suc != null)
                        {
                            estabID = (int)suc.PYF_EstablecimientoID;
                        }
                        if (resultado.MembresiaEstabID != estabID)
                        {
                            result = "NO ACTIVA EN SUCURSAL";
                            fechaUltimamembresia = ((DateTime)resultado.MembresiaPurchDate).ToString("dd/MM/yyyy h:mm tt");
                            fechaCaduca = ((DateTime)resultado.MembresiaEndDate).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            result = resultado.ESTADO;
                            fechaUltimamembresia = ((DateTime)resultado.MembresiaPurchDate).ToString("dd/MM/yyyy h:mm tt");
                            fechaCaduca = ((DateTime)resultado.MembresiaEndDate).ToString("dd/MM/yyyy");
                        }

                    }
                    break;
                }
                else
                {
                    if (i == 0) // toma la primera porque en el SP ya esta ordenado descendentemente por la fecha
                    {
                        result = resultado.ESTADO;
                        fechaUltimamembresia = ((DateTime)resultado.MembresiaPurchDate).ToString("dd/MM/yyyy h:mm tt");
                        fechaCaduca = ((DateTime)resultado.MembresiaEndDate).ToString("dd/MM/yyyy");
                    }
                    i++;
                }
            }
            if(result.CompareTo("INGRESO PERMITIDO") == 0)
            {
                fechaCaduca = "";
            }

            return result + "|" + fechaUltimamembresia + "|" + fechaCaduca;
        }


        public bool clientHasMembresias(string clientIdentification)
        {
            bool res = false;
            PagosyfacturasDataModelDataContext db = new PagosyfacturasDataModelDataContext();
            var resultados = db.getMembresias(ruc_empresa, clientIdentification);
            if (resultados.Count() > 0)
            {
                res = true;
            }
            return res;
        }


        public void deleteClientInPagosyFacturas(string clientIdentification)
        {
            try
            {
                DatamodelDataContext db = new DatamodelDataContext();
                var cli = (from cl in db.ACC_CLIENTEs
                           where cl.ID_CLIENTE == int.Parse(clientIdentification)
                           select cl).FirstOrDefault();
                if (cli != null)
                {
                    cli.ESTADO = "D";
                    db.SubmitChanges();
                }
            }
            catch (Exception ex) {

            }
            
            
        }





    }


    
}
