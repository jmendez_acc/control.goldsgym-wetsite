﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta name="google-signin-client_id" content="135993562667-adkjhg7qscmkbeslvmahdpe0tsbmsluj.apps.googleusercontent.com">
    <title>Goldsgym</title>
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <%--<!-- Add to homescreen for Safari on iOS -->--%>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="assets/images/favicon.ico">
    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="assets/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">
    <link rel="icon" sizes="192x192" href="../img/favicon.png" />
 
    <link rel="stylesheet" href="assets/css/material.cyan-light_blue.min.css?v=78743">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/alertify.min.css">
    <link rel="stylesheet" href="assets/css/themes/default.min.css">
   
    <link rel="stylesheet" href="assets/css/style.css?v=43f435">
  
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />

    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/material.min.js"></script>  
    <script src="assets/js/alertify.min.js"></script>  
    <script src="assets/js/bootstrap-notify.min.js"></script>

</head>
<body>
    
    <div id="general-wrapper-loading">

        <div class="loader" style="padding-top: 35%;">
                        <svg class="circular" viewBox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                        </svg>
                    </div>
    </div>
    <div id="general-wrapper" class="mdl-layout mdl-js-layout mdl-layout--drawer">
       
     <div class="demo-drawer mdl-layout__drawer  mdl-color-text--blue-grey-50">
            <div class="drawer-header-vertical-top-space">
                <div class="logo-space">
                    
                    <img class="logo-header" src="assets/images/logo.svg" />

                </div>
         
                
                  <div class="demo-avatar-dropdown">             
                          
                      <asp:Literal ID="literalProfilePicture" runat="server"></asp:Literal> 
                      <span class="avatar-email"><asp:Literal ID="literalProfileName" runat="server"></asp:Literal></span>                       
                      <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">  
                          <i class="material-icons" role="presentation">arrow_drop_down</i>                    
                       
                    </button>
                    
                    
                  </div>
            </div>
            <nav class="demo-navigation mdl-navigation">
                <asp:Literal ID="literalNav" runat="server"></asp:Literal>
                
              
                <div class="mdl-layout-spacer">
                </div>
            </nav>
        </div>


        
        <div class="overlay">

        </div>

         <div class="loader2">
              <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
              </svg>
    </div>
        <main class="mdl-layout__content">
            <div class="page-content">
                <iframe id="iframeContent" style="width: 100%; height: 100%; border: none; overflow-y: auto"></iframe>
            </div>
        </main>
         
        
    </div>
    
    
    <form id="Form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        <telerik:RadWindow ID="RadWindow1" runat="server" Skin="Bootstrap" RenderMode="Lightweight" OnClientClose="OnClientCloseHandler" DestroyOnClose="false" ShowContentDuringLoad="False"  Behaviors="Close,Move" VisibleStatusbar="false" VisibleTitlebar="true" OnClientDragStart="WindowDragStart" OnClientShow="OnRwindowClientShow" OnClientPageLoad="OnRwindowClientPageLoad"></telerik:RadWindow>
        <div id="loadingRadwindow" style="width: 50px; height: 50px; display: none; text-align: center; margin: auto;">
            <div class="loader">
              <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
              </svg>
            </div>
        </div>
        <telerik:RadCodeBlock runat="server">
            <script>
                var pageLoaded = 'modules/liveBiometrico.aspx?v=2';
                $(document).ready(function () {
                  
                    $(window).resize(function () {
                        resize();
                    });
                    resize();
                    setTimeout(function () {
                        $('#general-wrapper-loading').hide(function () {
                            $('#general-wrapper').css('visibility', 'visible');
                        });                        
                    }, 2000);
                    loadPage(pageLoaded);

                    //submenus
                    $('nav').on("click", "a", null, function () {
                        if ($(this).attr('id') != 'createprojectmnu') {
                            if ($(this).hasClass('parent-menu')) {
                                $('.submenu-navigation').slideUp();
                                if ($(this).hasClass('parent-menu-active')) {
                                    $('#' + $(this).attr('rel')).slideUp();
                                    $('nav a').removeClass('parent-menu-active');
                                    $('nav a').removeClass('active');
                                    $(this).find('.arrow-list-bullet').html('<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>');
                                    //$(this).addClass('parent-menu-active');
                                }
                                else {
                                    $('#' + $(this).attr('rel')).slideDown();
                                    $('nav a').removeClass('parent-menu-active');
                                    $('nav a').removeClass('active');
                                    $(this).addClass('parent-menu-active');
                                    $(this).find('.arrow-list-bullet').html('<span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>');
                                }

                            }
                            else {
                                $('nav a').removeClass('active');
                                $(this).addClass('active');
                                loadPage($(this).attr('rel'));
                                $('.mdl-layout__obfuscator').removeClass('is-visible');
                                $('.mdl-layout__drawer').removeClass('is-visible');
                            }
                        }
                    });

                });
                function resize() {
                    $('.mdl-grid.demo-content').height($(window).height() - 64);
                }
                
                function loadPage(_url) {
                    pageLoaded = _url;
                    $('.loader2').show();
                    $('.overlay').show();
                    $('#iframeContent').attr('src', _url);
                    
                }
                function reloadPage() {
                    loadPage(pageLoaded);
                }
                function hideLoader() {
                    $('.loader2').hide();
                    $('.overlay').hide();
                }
                function showLoader() {
                    $('.loader2').show();
                    $('.overlay').show();
                }
                function openWindow(_title, _url) {
                    var isMobile = false;
                    if ($(window).width() <= 1024) {
                        isMobile = true;
                    }
                    var radwindow = $find('<%=RadWindow1.ClientID %>');
                    radwindow.setUrl(_url);
                    radwindow.set_title("" + _title.toUpperCase());

                    radwindow.center();
                    radwindow.set_modal(true);
                    if (isMobile) {
                        radwindow.show();
                        radwindow.maximize();
                    }
                    else {
                        radwindow.set_minWidth(630);
                        radwindow.set_minHeight($(window).height()-60);
                        radwindow.set_height($(window).height() - 60);
                        radwindow.show();
                    }
                    customTelerikEvents();
                }
                function openWindowSmall(_title, _url) {
                    var isMobile = false;
                    if ($(window).width() <= 1024) {
                        isMobile = true;
                    }
                    var radwindow = $find('<%=RadWindow1.ClientID %>');
                    radwindow.setUrl(_url);
                    radwindow.set_title("&nbsp;&nbsp;&nbsp;" + _title);

                    radwindow.center();
                    radwindow.set_modal(true);
                    if (isMobile) {
                        radwindow.show();
                        radwindow.maximize();
                    }
                    else {
                        radwindow.set_minWidth(400);
                        radwindow.set_minHeight(230);
                        radwindow.set_height(230);
                        radwindow.show();
                    }
                    customTelerikEvents();
                }
                function openWindowSegment(_title, _url) {
                    var isMobile = false;
                    if ($(window).width() <= 1024) {
                        isMobile = true;
                    }
                    var radwindow = $find('<%=RadWindow1.ClientID %>');
                    radwindow.setUrl(_url);
                    radwindow.set_title("&nbsp;&nbsp;&nbsp;" + _title);

                    radwindow.center();
                    radwindow.set_modal(true);
                    if (isMobile) {
                        radwindow.show();
                        radwindow.maximize();
                    }
                    else {
                        radwindow.set_minWidth(470);
                        radwindow.set_minHeight(275);
                        radwindow.set_height(275);
                        radwindow.show();
                    }
                    customTelerikEvents();
                }
                function WindowDragStart(oWin, args) {
                   // var resizeExtender = oWin._resizeExtender;
                    //DISABLE iframe hiding 
                  //  resizeExtender.set_hideIframes(false);
                }
                var loadingSign = null;
                var contentCell = null;
                function OnRwindowClientShow(sender, args) {
                    
                    loadingSign = $("loadingRadwindow");
                    contentCell = sender._contentCell;
                    if (contentCell && loadingSign) {
                        
                        contentCell.appendChild(loadingSign);
                        contentCell.style.verticalAlign = "middle";
                        loadingSign.style.display = "";
                    }
                }
                function OnRwindowClientPageLoad(sender, args) {
                    if (contentCell && loadingSign) {
                        contentCell.removeChild(loadingSign);
                        contentCell.style.verticalAlign = "";
                        loadingSign.style.display = "none";
                    }
                }
                function closeWindow(_callback) {
                    var window = $find('<%=RadWindow1.ClientID %>');
                    window.close();
                    if (_callback != null) {
                        _callback();
                    }
                }
                function customTelerikEvents() {
                    $('.rwIcon').css('cursor', 'pointer');
                    $('.rwIcon').hide();
                    //only if not rwd
                    $('.TelerikModalOverlay').addClass("customized");
                    $('.TelerikModalOverlay').css("background-color", "#000");
                    $('.TelerikModalOverlay').css("opacity", "0.6");
                    $('.TelerikModalOverlay').css("height", "100%");
                    $('.RadWindow_Bootstrap').addClass('customized');
                    //$('.RadWindow_Bootstrap .rwTable .rwTitlebarControls').css('height', '45px');
                    $('.rwControlButtons').addClass('customized');
                    //$('.rwControlButtons').css('width','50px');
                    $('.RadWindow_Bootstrap .rwTable .rwControlButtons li').addClass('customized');
                    $('.RadWindow_Bootstrap .rwTable .rwControlButtons li').css('float', 'right');
                    $('.RadWindow_Bootstrap .rwTable .rwControlButtons li a').css('float', 'right');
                    $('.RadWindow_Bootstrap .rwTable .rwControlButtons li a').css('margin-left', '0px');
                    $('.RadWindow_Bootstrap .rwTable .rwControlButtons li a').css('margin-right', '-1px');
                    $('.RadWindow_Bootstrap .rwTable .rwTitlebarControls em').css('font-weight', '350');
                    $('.RadWindow_Bootstrap .rwTable .rwTitlebarControls em').css('font-family', 'Arial');


                    //$('.rwCloseButton').hide();
                    /*
                    $('.rwCloseButton').attr('href', '#');
                    $('.rwCloseButton').attr('onclick', 'alert(1);');
                    $('.rwCloseButton').on('click', function () {
                        alert(0);
                    });
                    */
                    //$('.rwCloseButton').click(closeWindow(null));
                }
                function viewAfiliado(id,name,periodoID) {
                    openWindow(name, "modules/Afiliado.aspx?id="+id+"&periodoid="+periodoID);
                }
                function viewClienteNoConfirmado(id,name,periodoID) {
                    openWindow(name, "modules/Cliente.aspx?id="+id+"&periodoid="+periodoID);
                }
                function viewEmailConfirmation(id, name) {
                    openWindow(name, "modules/AfiliadoConfirmarEmail.aspx?id="+id);
                }
                function createProjectAction() {
                    openWindow("CREATE NEW PROJECT", "modules/projectItem.aspx");
                }
                function createCompany() {
                    openWindowSmall("", "modules/createCompany.aspx");
                }
                function createZone() {
                    openWindowSmall("", "modules/createZone.aspx");
                }
                function createUser() {
                    openWindowSmall("", "modules/createUser.aspx");
                }
                function createSegment() {
                    openWindowSmall("", "modules/createSegment.aspx");
                }
                function createSubSegment() {
                    openWindowSmall("", "modules/createSubSegment.aspx");
                }
                function createResponsable() {
                    openWindowSmall("", "modules/createResponsable.aspx");
                }
                function segmentAssign(_ID,_title) {
                    openWindowSegment(_title, "modules/editSegment.aspx?id="+_ID);
                }
                function selectProjects() {
                    alertify.success('Nuevo Proyecto creado');
                    $('nav a').removeClass('active');
                    $('#projectsScreen').addClass('active');
                    loadPage('modules/Projects.aspx');
                }
                function selectCompanies() {
                    alertify.success('Nueva Empresa creada');
                    $('nav a').removeClass('active');
                    $('#companiesScreen').addClass('active');
                    loadPage('modules/Companies.aspx');
                }
                function selectZones() {
                    alertify.success('Nueva Zona creada');
                    $('nav a').removeClass('active');
                    $('#zonesScreen').addClass('active');
                    loadPage('modules/Zones.aspx');
                }
                function selectUsers() {
                    alertify.success('Nuevo Usuario creado');
                    $('nav a').removeClass('active');
                    $('#usersScreen').addClass('active');
                    loadPage('modules/Users.aspx');
                }
                function selectSegments() {
                    alertify.success('Nuevo Segmento creado');
                    $('nav a').removeClass('active');
                    $('#segmentsScreen').addClass('active');
                    loadPage('modules/Segments.aspx');
                }
                function selectSubSegments() {
                    alertify.success('Nuevo Sub-Segmento creado');
                    $('nav a').removeClass('active');
                    $('#subSegmentsScreen').addClass('active');
                    loadPage('modules/SubSegment.aspx');
                }
                function selectResponsables() {
                    alertify.success('Nuevo Responsable creado');
                    $('nav a').removeClass('active');
                    $('#responsablesScreen').addClass('active');
                    loadPage('modules/Responsables.aspx');
                }


                function refreshGridProjectSummary() {
                    document.getElementById('iframeContent').contentWindow.refreshGrid();
                }


                function notification(_message) {
                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', { allow_dismiss: true, delay: 10000, z_index: 999999999 });
                    notify.update({ type: 'info', message: _message, delay: 10000, z_index:999999999 });

                }
                function notificationRed(_message) {
                    var notify = $.notify('<strong>Saving</strong> Do not close this page...', { allow_dismiss: true, delay: 10000, z_index: 999999999 });
                    notify.update({ type: 'danger', message: _message, delay: 10000 });

                }
                function showLoader() {
                    $('.loader2').show();
                    $('.overlay').show();
                }
                
                function OnClientCloseHandler(sender, args) {
                    /*var data = args.get_argument();
                    if (data) {
                        alert(data);
                    }*/
                    document.getElementById('iframeContent').contentWindow.refreshGrid();
                }

                function changepassword() {
                    setTimeout(function () {
                        /*$('#general-wrapper-loading').hide(function () {
                            $('#general-wrapper').css('visibility', 'visible');
                        });*/
                        hideLoader();
                        
                                $('.mdl-layout__obfuscator').removeClass('is-visible');
                                $('.mdl-layout__drawer').removeClass('is-visible');
                    }, 1000);
                    loadPage('modules/changePassword.aspx');
                }
               
                
            </script>
            <style>
                .RadWindow_Bootstrap .rwTable .rwControlButtons li a:hover {
                    background-color: #EEEEEE;
                    border-color: #FFFFFF;
                    border-radius: 0px;
                }
                .mdl-layout__drawer{
                    width:280px;
                }
                
                .menu-label{
                    margin-left:20px;
                }
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link {
                    padding: 7px 25px;
                    margin: 0;
                    color: #FFFFFF;
                    outline:0;
                }
                .demo-layout .demo-navigation .mdl-navigation__link:hover {
                    background-color: #e1e1e1;
                    color: #37474F;
                    border-left: solid 5px #d24836;
                    padding-left: 15px;
                    text-decoration:none;
                }
                .demo-layout .demo-navigation .mdl-navigation__link.active {
                    border-left: solid 5px #d24836;
                    padding-left: 15px;
                    color:#222;
                    font-weight: bold;
                }
                .demo-layout .demo-navigation .mdl-navigation__link.active span{
                    color:#d24836;/*d24836*/
font-size: 1em;
                }
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:hover,
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:link,
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:active,
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:visited,
                .demo-layout .demo-navigation .mdl-navigation__link span:active,
                .demo-layout .demo-navigation .mdl-navigation__link span:hover,
                .demo-layout .demo-navigation .mdl-navigation__link span:link,
                .demo-layout .demo-navigation .mdl-navigation__link span:visited{
                    text-decoration:none;                    
                    border:none;
                }
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:hover,
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link:active{
                    background-color:#efe437;
                    color:#000000;
                }
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active,.mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active:active{
                    /*background-color:#39414e;*/
                    /*font-weight:bold;*/
                    color:#00f0ff;
                }

                .arrow-list-bullet{
      float: left;
      margin-left: -20px;
      font-size: 0.7em;
    }
    .arrow-list-bullet span{
        margin: 0px;
        margin-right: 8px;
        margin-top: 7px;
        margin-left: 6px;
        font-size: 0.8em;
    }
    .badge-list-bullet{
      float: left;
      margin-left: -20px;
    }
    .badge-list-bullet span{
      margin:0px;
      font-weight: normal;
      margin-right: 10px;
    }


                .submenu-navigation{
      background-color:#1e1e1e;
      display: none;
      border-top:1px solid rgba(255,255,255,.06);
      border-bottom:1px solid rgba(255,255,255,.06);

    }
    .demo-layout .demo-navigation .mdl-navigation__link:hover, .demo-layout .demo-navigation  .submenu-navigation .mdl-navigation__link.active:hover {
      text-decoration: none;
      padding-right: 15px;
      font-size: 0.9em;
    }
    .demo-layout .demo-navigation .mdl-navigation__link, .demo-layout .demo-navigation  .submenu-navigation .mdl-navigation__link.active {
      text-decoration: none;
      padding-right: 15px;
      font-size: 0.9em;
    }
    .demo-layout .demo-navigation .submenu-navigation .mdl-navigation__link:hover, .demo-layout .demo-navigation  .submenu-navigation .mdl-navigation__link.active:hover {
      font-size: 0.8em;
      text-decoration: none;
      padding-right: 15px;
    }
    .demo-layout .demo-navigation .submenu-navigation .mdl-navigation__link, .demo-layout .demo-navigation  .submenu-navigation .mdl-navigation__link.active {
      font-size: 0.8em;
      text-decoration: none;
      padding-right: 15px;
    }

.parent-menu-active{
  background-color:#1e1e1e;
}

.demo-layout .demo-navigation .parent-menu.mdl-navigation__link:hover, .demo-layout .demo-navigation .parent-menu.mdl-navigation__link.active:hover{
  background-color:#1C3C44;
  color:#FFFFFF;
}
.demo-layout .demo-navigation .parent-menu.mdl-navigation__link:hover i, .demo-layout .demo-navigation .parent-menu.mdl-navigation__link.active:hover i{
  color:#FFFFFF;
}

.submenu-navigation span{
    font-size:0.9em;
}

.overlay{
    width: 100%;
height: 100%;
background-color: #FFFFFF;
position: fixed;
z-index: 99999;
display:none;
opacity:0.7;
}

                .avatar-email{
                    color:#FFF;
                    margin-left: 10px;
font-size: 0.9em;
                }


                .demo-avatar-dropdown{
                   /* margin-left:15px;
                    margin-top: 18px;*/
                }
                .logo-space{
                  
  width: 100%;
height: auto;
padding-top: 0px;
background-color: #efe437;
                }

                .RadWindow_Bootstrap .rwTitleBar .rwTitleWrapper {
    min-height: 0px;
    padding: .42857em .71429em;
    border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: #FFF;
    border: none;
    background: #FFF;
    padding-top: 15px;
padding-bottom: 15px;
padding-left: 0px;
}
                .RadWindow_Bootstrap .rwTitleWrapper .rwTitle {
    font-size: 1.3em;
    font-weight: normal;
    margin: .28571em 0 0;
    padding-left: 15px;
   }

                
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active, .mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active:hover {
    color: #000;
}
                .mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active, .mdl-layout__drawer .mdl-navigation .mdl-navigation__link.active:active {
    color: #efe437;
}

                .mdl-navigation__link.parent-menu .fa{
                    font-size:17px;
                }


            </style>
        </telerik:RadCodeBlock>
         <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="accbtn">
             <li class="mdl-menu__item" onclick="changepassword();">
                          <span class="glyphicon glyphicon-exit" aria-hidden="true"></span>Cambiar contraseña
                          <div class="g-signin2" data-onsuccess="onSignIn" style="display:none"></div>
                      </li>
             <li class="mdl-menu__item" onclick="logout();">
                          <span class="glyphicon glyphicon-exit" aria-hidden="true"></span>Salir de sesión
                          <div class="g-signin2" data-onsuccess="onSignIn" style="display:none"></div>
                      </li>
                    </ul>
    </form>
    <script>


        function logout() {
            //window.location.href = "Logout/";
             window.location.href = "Logout.aspx";
        }

    </script>
 
</body>
</html>
