﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector.objects
{
    public class Ride
    {
        public string ClaveAccesso { get; set; }
        public byte[] PDF { get; set; }
    }
}
