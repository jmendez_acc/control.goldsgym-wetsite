﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changePassword.aspx.cs" Inherits="ADMIN_administrador_changePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../assets/js/jquery-1.11.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <link href="../assets/css/style.css?v=6" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
          <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.module-content-grid').css('visibility', 'visible');
                    parent.hideLoader();
                });
            </script>

        </telerik:RadCodeBlock>



        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" Runat="server" Skin="MetroTouch" DecoratedControls="All" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadButton1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txt_clave" />
                        <telerik:AjaxUpdatedControl ControlID="txt_re_clave" />
                        <telerik:AjaxUpdatedControl ControlID="txt_clave_actual" UpdatePanelCssClass="" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="BlackMetroTouch">
        </telerik:RadAjaxLoadingPanel>















        <div class="row module-header">
            <div class="col-xs-8 text-left">
                <h3>CAMBIAR CONTRASEÑA
                </h3>
            </div>
            <div class="col-xs-4 text-right" runat="server" id="createProyectActionDiv" style="margin-top: -11px">
            </div>
        </div>

        <div class="module-content-grid" style="visibility: visible">
            <div class="container module">
                <div class="row">
                    <div class="col-xs-12">
                        <br />
                        <br />
                        <br />
                    <table style="width:400px; margin:0 auto">
                        <tr>
                            <td>
                                <label>Clave Actual:</label>
                                <br />
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txt_clave_actual" runat="server" TextMode="Password" Width="200px"></telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Clave nueva:</label>
                                <br />
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txt_clave" runat="server" TextMode="Password" Width="200px"></telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label>Repetir clave nueva:
                                </label>
                                <br />
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txt_re_clave" runat="server" TextMode="Password" Width="200px"></telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                <br />
                                <telerik:RadButton ID="RadButton1" runat="server" Text="Cambiar" Width="200px" Skin="MetroTouch" OnClick="RadButton1_Click"></telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                    <br />
                        <br />
                        <br />
                    </div>
                </div>
                
            </div>
        </div>
    </form>
</body>
</html>
