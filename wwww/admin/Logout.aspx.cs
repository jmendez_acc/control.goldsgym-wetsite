﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["admin_user_id"] = null;
            Session["admin_user_name"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Redirect("~/admin/login");

        }
    }
}