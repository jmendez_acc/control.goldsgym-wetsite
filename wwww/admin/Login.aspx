﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CONTROL Goldsgym</title>
    <link rel="icon" sizes="192x192" href="../img/favicon.png" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/material.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
</head>
<body>
    <form id="form1" runat="server">
        
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>

        <div class="banner-top"></div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <br />
                    <br />
                    <br />
                    <img class="logo" src="assets/images/logo.svg" />
                    <br />
                    <br />
                    <br />
                    <label style="width:350px;text-align:left">NOMBRE DE USUARIO:</label><br />
                    <telerik:RadTextBox ID="txtUsername" runat="server" Skin="Material" EmptyMessage="" RenderMode="Lightweight" Width="350px"></telerik:RadTextBox>
                    <br />
                    <br />
                    <br />
                    <label style="width:350px;text-align:left">CONTRASEÑA:</label><br />
                    <telerik:RadTextBox ID="txtPassword" runat="server" Skin="Material" EmptyMessage="" TextMode="Password" RenderMode="Lightweight" Width="350px"></telerik:RadTextBox>
                    <br />
                    <br /><br />
                    <telerik:RadButton runat="server" ID="btnLogin" Text="INGRESAR" Skin="Material" Width="350px" RenderMode="Lightweight" OnClick="btnLogin_Click"></telerik:RadButton>
                    <br />
                    <br />
                    <div class="domain_name">[control.goldsgym.com.ec]</div>
                    <br />
                    <asp:Label ID="lblError" Visible="false" ForeColor="Red" runat="server" Text="Label"></asp:Label>
                    <br />
                    <br />
                </div>
                
            </div>
        </div>
        
                    
        <div class="banner-bottom"></div>
        

    </form>
    <style>
        .banner-top{
            background:#efe437;
            border-bottom:5px solid #000000;
            width:100%;
            height:20px;
            margin:0px;
            padding:0px;
        }
        .banner-bottom{
            background:#efe437;
            border-top:5px solid #000000;
            width:100%;
            height:20px;
            margin:0px;
            padding:0px;
            position:fixed;
            bottom:0px;
            left:0px;
        }
        .logo{
            width:150px;
        }
        label{
            margin-bottom:-5px;
            font-size: 1.1em;
            color:#444;
        }
        .RadInput_Material input.riTextBox {
            padding-top: 4px;
        }
        .domain_name{
            letter-spacing: 2px;
            font-size: 0.9em;
            position: relative;
            width: 100%;
            text-align: center;
            color: #575757;
            font-weight: bold;
        }
    </style>
</body>
</html>
