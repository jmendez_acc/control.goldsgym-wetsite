﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;

/// <summary>
/// Summary description for Notifications
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class Notifications : System.Web.Services.WebService
{

    public Notifications()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string recoverEmail(string _correo) {
        string res = "OK";

       string correo = _correo.Trim();
        try
        {
            goldsgymEntities db = new goldsgymEntities();

            Inscripcione ins = db.Inscripciones.FirstOrDefault(p => p.Correo == correo);
            if (ins != null)
            {


                //envia la clave de inscripciones
                //SENDS EMAIL

                //------------------------- para user   ------------------------
                //MailMessage mail2 = new MailMessage(WebConfigurationManager.AppSettings["smtp_from"], txtEmail.Text.Trim());
                MailMessage mail2 = new MailMessage(new MailAddress(WebConfigurationManager.AppSettings["smtp_from"], "GOLD'S GYM ECUADOR"), new MailAddress(correo));

                SmtpClient client2 = new SmtpClient();
                NetworkCredential basic2 = new NetworkCredential(WebConfigurationManager.AppSettings["smtp_user"], WebConfigurationManager.AppSettings["smtp_password"]);
                client2.UseDefaultCredentials = false;
                client2.Credentials = basic2;
                client2.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["requireSSL"]);
                client2.Port = Int16.Parse(WebConfigurationManager.AppSettings["smtp_port"]);
                client2.DeliveryMethod = SmtpDeliveryMethod.Network;
                client2.Host = WebConfigurationManager.AppSettings["smtp_host"];
                mail2.IsBodyHtml = true;
                mail2.Body = @"
         <!DOCTYPE html>
 <html lang='en' >

 <head>
   <meta charset='UTF-8'>
   <title>RESET PASSWORD</title>





 </head>

 <body>

   <head>
   <title></title>
 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
 <style type='text/css'>
   #outlook a { padding: 0; }
   .ReadMsgBody { width: 100%; }
   .ExternalClass { width: 100%; }
   .ExternalClass * { line-height:100%; }
   body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
   table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
   img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
   p { display: block; margin: 13px 0; }
 </style>
 <!--[if !mso]><!-->
 <style type='text/css'>
   @media only screen and (max-width:480px) {
     @-ms-viewport { width:320px; }
     @viewport { width:320px; }
   }
 </style>
 <!--<![endif]-->
 <!--[if mso]>
 <xml>
   <o:OfficeDocumentSettings>
     <o:AllowPNG/>
     <o:PixelsPerInch>96</o:PixelsPerInch>
   </o:OfficeDocumentSettings>
 </xml>
 <![endif]-->
 <style type='text/css'>
   @media only screen and (min-width:480px) {
     .mj-column-per-100, * [aria-labelledby='mj-column-per-100'] { width:100%!important; }
 .mj-column-per-80, * [aria-labelledby='mj-column-per-80'] { width:80%!important; }
 .mj-column-per-30, * [aria-labelledby='mj-column-per-30'] { width:30%!important; }
 .mj-column-per-70, * [aria-labelledby='mj-column-per-70'] { width:70%!important; }
   }
 </style>
 </head>
 <body style='background: #E3E5E7;'>
   <div style='background-color:#E3E5E7;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;' align='center'><table cellpadding='0' cellspacing='0' style='border-collapse:collapse;border-spacing:0px;' align='center' border='0'><tbody><tr><td style='width:100px;'><a href='about:blank' target='_blank'><img alt='auth0' title='' height='auto' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/logo-email-template.png' style='border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;' width='80'></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:#222228;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#222228;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:480px;'>
       <![endif]--><div aria-labelledby='mj-column-per-80' class='mj-column-per-80' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;padding-top:30px;' align='center'><table cellpadding='0' cellspacing='0' style='border-collapse:collapse;border-spacing:0px;' align='center' border='0'><tbody><tr><td style='width:80px;'><img alt='goldsgym' title='' height='auto' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/password.png' style='border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;' width='80'></td></tr></tbody></table></td></tr><tr><td style='word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;' align='center'><div style='cursor:auto;color:white;font-family:Avenir Next, Avenir, sans-serif;font-size:32px;line-height:60ps;'>
            Contraseña
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:40px 25px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 25px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:18px;font-weight:500;line-height:30px;'>
           Registramos su acceso con los siguientes datos:
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:180px;'>
       <![endif]--><div aria-labelledby='mj-column-per-30' class='mj-column-per-30' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
             <strong style='font-weight: 500; white-space: nowrap;'>Email</strong>
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:420px;'>
       <![endif]--><div aria-labelledby='mj-column-per-70' class='mj-column-per-70' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
            " + correo + @"
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:180px;'>
       <![endif]--><div aria-labelledby='mj-column-per-30' class='mj-column-per-30' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
             <strong style='font-weight: 500; white-space: nowrap;'>Contraseña</strong>
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:420px;'>
       <![endif]--><div aria-labelledby='mj-column-per-70' class='mj-column-per-70' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 25px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
            " + ins.Password + @"
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:0px 30px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:undefined;width:600px;'>
       <![endif]--><p style='font-size:1px;margin:0 auto;border-top:1px solid #E3E5E7;width:100%;'></p><!--[if mso | IE]><table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size:1px;margin:0 auto;border-top:1px solid #E3E5E7;width:100%;' width='600'><tr><td style='height:0;line-height:0;'> </td></tr></table><![endif]--><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;' align='center'><table cellpadding='0' cellspacing='0' align='center' border='0'><tbody><tr><td style='border-radius:3px;color:white;cursor:auto;' align='center' valign='middle' bgcolor='#020202'><a href='https://www.goldsgym.com.ec' style='display:inline-block;text-decoration:none;background:#020202;border-radius:3px;color:white;font-family:Avenir Next, Avenir, sans-serif;font-size:14px;font-weight:500;line-height:35px;padding:10px 25px;margin:0px;' target='_blank'>
            IR AL WEBSITE
           </a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 25px 15px;' align='justify'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>


              <img alt='' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/slogan-losmejores.png' />
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:#F5F7F9;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#F5F7F9;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 20px;' align='justify'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:13px;line-height:20px;'>
           Recomendamos como buena práctica de seguridad cambiar su contraseña, luego de ingresar al panel de clientes en el enlace de Editar Perfil.
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></div>
 <br/>


 </body>



 </body>

 </html>



                                 ";
                mail2.Subject = "GOLD'S GYM ECUADOR - RECORDATORIO DE CLAVE";
                client2.Send(mail2);

                

            }
            else
            {
                //busca en pagos y facturas
                PagosyFacturasConnector.PagosyfacturasController plugin = new PagosyFacturasConnector.PagosyfacturasController();
                PagosyFacturasConnector.objects.Customer cust = plugin.getCustomerFromPagosyFacturasByOnlyMail(correo);
                if (cust != null)
                {

                    //envia la clave de cust
                    //------------------------- para user   ------------------------
                    //MailMessage mail2 = new MailMessage(WebConfigurationManager.AppSettings["smtp_from"], txtEmail.Text.Trim());
                    MailMessage mail2 = new MailMessage(new MailAddress(WebConfigurationManager.AppSettings["smtp_from"], "GOLD'S GYM ECUADOR"), new MailAddress(correo));

                    SmtpClient client2 = new SmtpClient();
                    NetworkCredential basic2 = new NetworkCredential(WebConfigurationManager.AppSettings["smtp_user"], WebConfigurationManager.AppSettings["smtp_password"]);
                    client2.UseDefaultCredentials = false;
                    client2.Credentials = basic2;
                    client2.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["requireSSL"]);
                    client2.Port = Int16.Parse(WebConfigurationManager.AppSettings["smtp_port"]);
                    client2.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client2.Host = WebConfigurationManager.AppSettings["smtp_host"];
                    mail2.IsBodyHtml = true;
                    mail2.Body = @"
         <!DOCTYPE html>
 <html lang='en' >

 <head>
   <meta charset='UTF-8'>
   <title>RESET PASSWORD</title>





 </head>

 <body>

   <head>
   <title></title>
 <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
 <style type='text/css'>
   #outlook a { padding: 0; }
   .ReadMsgBody { width: 100%; }
   .ExternalClass { width: 100%; }
   .ExternalClass * { line-height:100%; }
   body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
   table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
   img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
   p { display: block; margin: 13px 0; }
 </style>
 <!--[if !mso]><!-->
 <style type='text/css'>
   @media only screen and (max-width:480px) {
     @-ms-viewport { width:320px; }
     @viewport { width:320px; }
   }
 </style>
 <!--<![endif]-->
 <!--[if mso]>
 <xml>
   <o:OfficeDocumentSettings>
     <o:AllowPNG/>
     <o:PixelsPerInch>96</o:PixelsPerInch>
   </o:OfficeDocumentSettings>
 </xml>
 <![endif]-->
 <style type='text/css'>
   @media only screen and (min-width:480px) {
     .mj-column-per-100, * [aria-labelledby='mj-column-per-100'] { width:100%!important; }
 .mj-column-per-80, * [aria-labelledby='mj-column-per-80'] { width:80%!important; }
 .mj-column-per-30, * [aria-labelledby='mj-column-per-30'] { width:30%!important; }
 .mj-column-per-70, * [aria-labelledby='mj-column-per-70'] { width:70%!important; }
   }
 </style>
 </head>
 <body style='background: #E3E5E7;'>
   <div style='background-color:#E3E5E7;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;' align='center'><table cellpadding='0' cellspacing='0' style='border-collapse:collapse;border-spacing:0px;' align='center' border='0'><tbody><tr><td style='width:100px;'><a href='about:blank' target='_blank'><img alt='auth0' title='' height='auto' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/logo-email-template.png' style='border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;' width='80'></a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:#222228;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#222228;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:480px;'>
       <![endif]--><div aria-labelledby='mj-column-per-80' class='mj-column-per-80' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;padding-top:30px;' align='center'><table cellpadding='0' cellspacing='0' style='border-collapse:collapse;border-spacing:0px;' align='center' border='0'><tbody><tr><td style='width:80px;'><img alt='goldsgym' title='' height='auto' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/password.png' style='border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:auto;' width='80'></td></tr></tbody></table></td></tr><tr><td style='word-break:break-word;font-size:0px;padding:0px 20px 0px 20px;' align='center'><div style='cursor:auto;color:white;font-family:Avenir Next, Avenir, sans-serif;font-size:32px;line-height:60ps;'>
            Contraseña
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:40px 25px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 25px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:18px;font-weight:500;line-height:30px;'>
           Registramos su acceso con los siguientes datos:
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:180px;'>
       <![endif]--><div aria-labelledby='mj-column-per-30' class='mj-column-per-30' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
             <strong style='font-weight: 500; white-space: nowrap;'>Email</strong>
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:420px;'>
       <![endif]--><div aria-labelledby='mj-column-per-70' class='mj-column-per-70' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
            " + correo + @"
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:180px;'>
       <![endif]--><div aria-labelledby='mj-column-per-30' class='mj-column-per-30' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 10px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
             <strong style='font-weight: 500; white-space: nowrap;'>Contraseña</strong>
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td><td style='vertical-align:top;width:420px;'>
       <![endif]--><div aria-labelledby='mj-column-per-70' class='mj-column-per-70' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 25px;' align='left'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>
            " + cust.Identificacion + @"
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:0px 30px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:undefined;width:600px;'>
       <![endif]--><p style='font-size:1px;margin:0 auto;border-top:1px solid #E3E5E7;width:100%;'></p><!--[if mso | IE]><table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size:1px;margin:0 auto;border-top:1px solid #E3E5E7;width:100%;' width='600'><tr><td style='height:0;line-height:0;'> </td></tr></table><![endif]--><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;' align='center'><table cellpadding='0' cellspacing='0' align='center' border='0'><tbody><tr><td style='border-radius:3px;color:white;cursor:auto;' align='center' valign='middle' bgcolor='#020202'><a href='https://www.goldsgym.com.ec' style='display:inline-block;text-decoration:none;background:#020202;border-radius:3px;color:white;font-family:Avenir Next, Avenir, sans-serif;font-size:14px;font-weight:500;line-height:35px;padding:10px 25px;margin:0px;' target='_blank'>
            IR AL WEBSITE
           </a></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:white;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:white;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 25px 15px;' align='justify'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:16px;line-height:30px;'>


              <img alt='' src='https://www.pagosyfacturas.com/uploads/notifications/0992222980001/slogan-losmejores.png' />
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div style='margin:0 auto;max-width:600px;background:#F5F7F9;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#F5F7F9;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;'><!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0'><tr><td style='vertical-align:top;width:600px;'>
       <![endif]--><div aria-labelledby='mj-column-per-100' class='mj-column-per-100' style='vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;'><table cellpadding='0' cellspacing='0' style='vertical-align:top;' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 20px;' align='justify'><div style='cursor:auto;color:#222228;font-family:Avenir Next, Avenir, sans-serif;font-size:13px;line-height:20px;'>
           Recomendamos como buena práctica de seguridad cambiar su contraseña, luego de ingresar al panel de clientes en el enlace de Editar Perfil.
           </div></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]-->
       <!--[if mso | IE]>
       <table border='0' cellpadding='0' cellspacing='0' width='600' align='center' style='width:600px;'>
         <tr>
           <td style='line-height:0px;font-size:0px;mso-line-height-rule:exactly;'>
       <![endif]--><div></div><!--[if mso | IE]>
       </td></tr></table>
       <![endif]--></div>
 <br/>


 </body>



 </body>

 </html>



                                 ";
                    mail2.Subject = "GOLD'S GYM ECUADOR - RECORDATORIO DE CLAVE";
                    client2.Send(mail2);

                }
                else {
                    res = "NOT_REGISTERED";
                }
            }


        }
        catch (Exception ex)
        {
            res = ex.Message;
         }
        return res;
    }

    [WebMethod]
    public string getPersona(string personID)
    {
        PagosyFacturasConnector.goldsgymDataContext db = new PagosyFacturasConnector.goldsgymDataContext();
        PagosyFacturasConnector.Inscripcione persona = db.Inscripciones.FirstOrDefault(p=>p.PersonID.CompareTo(personID)==0);
        if (persona != null)
        {
            return persona.Identificacion + "|" + persona.Nombre +" "+ persona.Apellido;
        }
        else
        {
            return "0|error";
        }
    }
}
