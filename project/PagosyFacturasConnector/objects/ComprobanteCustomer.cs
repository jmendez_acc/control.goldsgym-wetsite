﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagosyFacturasConnector.objects
{
    public class ComprobanteCustomer
    {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public string NroComprobante { get; set; }
        public string Detalle { get; set; }
        public string Subtotal { get; set; }
        public string Iva { get; set; }
        public string Total { get; set; }
        public string ClaveAcceso { get; set; }
    }
}
