﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HandlerErrors
/// </summary>
public class HandlerErrors
{
    public HandlerErrors()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void Log(string mensaje,string identificacion,int sucursalID,int UserID)
    {
        goldsgymEntities db = new goldsgymEntities();
        LogErrore log = new LogErrore();
        log.Message = mensaje;
        log.Identification = identificacion;
        log.SucursalID = sucursalID;
        log.UserID = UserID;
        log.Entry = DateTime.Now;
        db.LogErrores.Add(log);
        db.SaveChanges();
    }
}