﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        /*Session["admin_role_id"] = "1";
        Session["admin_user_id"] = "1";
        Session["admin_user_name"] = "fasdf";
        Response.Redirect("~/Admin/");
        */
        goldsgymEntities db = new goldsgymEntities();
        string enc = encripta.encrypt(txtPassword.Text);
        var usuario = (from u in db.user_admin
                       where u.usuario == txtUsername.Text
                       where u.clave == enc
                       select u).FirstOrDefault();
        //acc_usuario usuario = new acc_usuario();
        //usuario.usuario = "admin";
        //  FormsAuthentication.RedirectFromLoginPage(txt_username.Text, false);
        //  Response.Redirect("~/ADMIN/administrador/Default.aspx");

        if (usuario != null && usuario.usuario.Length > 1)
        {
            Session["IsAdmin"] = "1";
            //Session["admin_role_id"] = "1";
            Session["admin_user_id"] = usuario.ID.ToString();
            Session["admin_user_name"] = usuario.usuario;
            FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
            Response.Redirect("~/admin");
        }
        else
        {
            
            //va a buscar a los cajeros
            PagosyFacturasConnector.PagosyfacturasDataModelDataContext dbx = new PagosyFacturasConnector.PagosyfacturasDataModelDataContext();
            PagosyFacturasConnector.ACC_USUARIO usuario_cajero = dbx.ACC_USUARIOs.FirstOrDefault(p => p.id_parent == 21 && p.usuario.CompareTo(txtUsername.Text) == 0 && p.clave.CompareTo(encripta.encrypt(txtPassword.Text)) == 0);

            if (usuario_cajero != null)
            {
                Session["IsAdmin"] = "0";
                Session["admin_user_id"] = usuario_cajero.id_usuario.ToString();
                Session["admin_user_name"] = usuario_cajero.usuario;
                FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
                Response.Redirect("~/admin");
            }

            else
            {
                txtPassword.Text = string.Empty;
                txtUsername.Text = string.Empty;

                lblError.Text = "Usuario o contraseña incorrecta.";
            }




        }
        /*DbAccess.fetDataContext db = new DbAccess.fetDataContext();
        DbAccess.usuario user = db.usuarios.FirstOrDefault(p => p.Username == txtUsername.Text.Trim() && p.Password == txtPassword.Text.Trim());
        if (user != null)
        {
            Session["admin_role_id"] = "1";
            Session["admin_user_id"] = user.ID.ToString();
            Session["admin_user_name"] = user.Username;
            Response.Redirect("~/Admin/");
        }
        else
        {
            lblError.Text = "**USUARIO O CONTRASEÑA INCORRECTA(S)**";
            lblError.Visible = true;
        }*/
    }
}